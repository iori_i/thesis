# What is a paraphrase?のメモ

## Abstruct

Paraphrases are sentences or phrases that convey the same meaning using different wording.
言い換えは異なる単語使って同じ意味を伝える文やフレーズのこと．

Although the logical definition of paraphrases requires strict semantic equivalence, linguistics accepts a broader, approximate, equivalence—thereby allowing far more examples of “quasi-paraphrase.”
言い換えの論理的な定義では厳密な意味の等価性が求められるが，言語学ではより広い類似を受け入れ，それによってより多くの疑似言い換えの例を認めることができる．

But approximate equivalence is hard to define. 
しかし類似等価性を定義することは難しい．

Thus, the phenomenon of paraphrases, as understood in linguistics, is difficult to characterize. 
したがって，言語学で理解されているように言い換えを特徴づけることは難しい．

In this article, we list a set of 25 operations that generate quasi-paraphrases.
この論文では，疑似言い換えを生成する25の操作を述べる．

We then empirically validate the scope and accuracy of this list by manually analyzing random samples of two publicly available paraphrase corpora. 
公開されている2つの言い換えコーパスをランダムにサンプリングし，手動で分析することによりリストの精度と規模を評価する．

We provide the distribution of naturally occurring quasi-paraphrases in English text.
自然に発生する英語の疑似言い換えの分布を提供する．


## Introduction

異なる表現を使用して同じ意味を伝える文またはフレーズを言い換えと呼ぶ．

テキストの含意，機械読解，質問応答，情報抽出，機械翻訳などのNLPタスクで重要
テキストに同じことが複数含まれているとき，それらに対して同じ選択をするには自動化された言い換え認識の手法が有用

言い換え認識が難しい理由として，言い換えの定義が難しいから
「言い換え」の厳密な定義は全く同じ意味を必要とするため非常に狭くなるが，言語学の文献では言い回しはほとんどの場合，文または句全体の意味のおおよその等価性によって特徴づけられている

De Beaugrande and Dressler (1981, page 50)は言い換えを 外見的に異なる素材感の概念的な等価性の近似と定義している

Hirst (2003, slide 9) は言い換えを異なる方法で同じ状況について話すことと定義している
彼は言い換えは完全に同義語ではないと主張し，言い換えには評価，意味合い，視点などの違いがあるとしている

Mel'cuk (2012, page 7) によると文の同義語は同じSemSから生成されるとし，おおよその言い換えには含意が含まれると付け加えた

Clark (1992, page 172) は 2つの形式はすべて意味が対象的であると 絶対同義語であると考え方を拒否した

全体として，言語学の文献には言い換えが厳密な同義語に限定されないと主張する多くの研究がある

この論文では言い換えを広く見ていく
論理的に理解されている厳密な言い換えの概念と言語学の幅広い概念との間の矛盾を避けるため，疑似言い換え（準言い換え）という用語を使用し，この記事での言い換えはそれを指す
疑似言い換えは 異なる単語を使用してほぼ同じ意味を伝える文もしくはフレーズとする

The school said that their buses seat 40 students each.
The school said that their buses cram in 40 students each.
seatは席，cramは混雑しているといった文で，seatとcramは同義語ではない
彼らは同じ状況について話者によって異なる評価をしている
これを疑似言い換えとする

The school said that their buses seat 40 students each.
The school is saying that their buses might accommodate 40 students each.
これらは時制が異なるが，疑似言い換えとみなす


## 言い換えの分類

1. (Synonym substitution)同義語の置き換え
Google bought YouTube. ⇔ Google acquired YouTube.
Chris is slim. ⇔ Chris is slender. ⇔ Chris is skinny.

2. Antonym substitution(対義語の置き換え)
Pat ate. ⇔ Pat did not starve.

3. Converse substitution(逆の視点からみる)
Google bought YouTube. ⇔ YouTube was sold to Google.

4. Change of voice(受動態能動態の変換)
Pat loves Chris. ⇔ Chris is loved by Pat.

5. Change of person(人の変更)
Pat said, “I like football.” ⇔ Pat said that he liked football.

6. Pronoun/Co-referent substitution(代名詞・共参照の置き換え)
Pat likes Chris, because she is smart. ⇔ Pat likes Chris, because Chris is smart.

7. Repetition/Ellipsis(繰り返しと省略)
Pat can run fast and Chris can run fast, too. ⇔ Pat can run fast and Chris can, too.

8. Function word variations(機能語のバリエーション)
Results of the competition have been declared. ⇔ Results for the competition have been declared.
Pat showed a nice demo. ⇔ Pat's demo was nice.

9. Actor/Action substitution(アクションの置き換え)
I dislike rash drivers. ⇔ I dislike rash driving.

10. Verb/“Semantic-role noun” substitution(動詞・主題役割名詞の置き換え)
Pat teaches Chris. ⇔ Pat is Chris's teacher.
Pat teaches Chris. ⇔ Chris is Pat's student.
Pat tiled his bathroom floor. ⇔ Pat installed tiles on his bathroom floor.

11. Manipulator/Device substitution(マニピュレータと装置の置き換え)
The pilot took off despite the stormy weather. ⇔ The plane took off despite the stormy weather.

12. General/Specific substitution(一般・特殊/特定な単語の置き換え)
I dislike rash drivers. ⇔ I dislike rash motorists.
Pat is flying in this weekend. ⇔ Pat is flying in this Saturday.

13. Metaphor substitution(比喩表現)
I had to drive through fog today. ⇔ I had to drive through a wall of fog today.
Immigrants have used this network to send cash. ⇔ Immigrants have used this network to send stashes of cash.

14. Part/Whole substitution(一部と全部の置き換え)
American airplanes pounded the Taliban defenses. ⇔ American airforce pounded the Taliban defenses.

15. Verb/Noun conversion(動詞・名詞の変換)
The police interrogated the suspects. ⇔ The police subjected the suspects to an interrogation.
The virus spread over two weeks. ⇔ Two weeks saw a spreading of the virus.

16. Verb/Adjective conversion(動詞・形容詞の変換)
Pat loves Chris. ⇔ Chris is lovable to Pat.

17. Verb/Adverb conversion(動詞・副詞の変換)
Pat boasted about his work. ⇔ Pat spoke boastfully about his work.

18. Noun/Adjective conversion(名詞と形容詞の変換)
I'll fly by the end of June. ⇔ I'll fly late June.

19. Verb-preposition/Noun substitution(動詞の前置詞と名詞の置き換え)
The finalists will play in Giants stadium. ⇔ Giants stadium will be the playground for the finalists.

20. Change of tense(時制の変化)
Pat loved Chris. ⇔ Pat loves Chris.

21. Change of aspect(動詞のアスペクトの変化)
Pat is flying in today. ⇔ Pat flies in today.

* アスペクトとは
動詞のアスペクトはアクションが進行中か完了しているかによって決まり，以下の四種類に分類される
- Simple Aspect:              事実を示す
- Perfect Aspect:             完了したアクションを示す
- Progressive Aspect:         進行中のアクションを示す
- Perfect Progressive Aspect: 完了した進行中のアクションを示す

22. Change of modality(モダリティの変化)
Google must buy YouTube. ⇔ Google bought YouTube.
The government wants to boost the economy. ⇔ The government hopes to boost the economy.

23. Semantic implication(意味の含意)
Google is in talks to buy YouTube. ⇔ Google bought YouTube.
The Marines are fighting the terrorists. ⇔ The Marines are eliminating the terrorists.

24. Approximate numerical equivalences(近似的な数値の等価性)
At least 23 U.S. soldiers were killed in Iraq last month. ⇔ About 25 U.S. soldiers were killed in Iraq last month.
Disneyland is 32 miles from here. ⇔ Disneyland is around 30 minutes from here.

25. External knowledge(外部知識)
We must work hard to win this election. ⇔ The Democrats must work hard to win this election.
The government declared victory in Iraq. ⇔ Bush declared victory in Iraq.


## 分析
1. 分布
   上で分類した言い換えに対してそれぞれの分布はどの様になっているか
2. 人による評価
   語句を変えていった場合，どのくらい疑似言い換えが生成可能か

言い換えの分類とそれぞれの精度の分布表
WordNetを使用して単語を置き換えて言い換えを生成し，人手で評価
κ統計量0.66
